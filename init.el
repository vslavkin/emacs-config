;; -*- lexical-binding: t -*-

(setq-default custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file) ;; If exists, load it
  (load custom-file))

;; Put autosave files (ie #foo#) and backup files (ie foo~) in ~/.emacs.d/.
(setq
 auto-save-file-name-transforms '((".*" "~/.emacs.d/autosaves/\\1" t))
 backup-directory-alist '((".*" . "~/.emacs.d/backups/")))

;; create the autosave dir if necessary, since emacs won't.
(make-directory "~/.emacs.d/autosaves/" t)

(use-package emacs
  :ensure nil
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
		    (replace-regexp-in-string
		     "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
		     crm-separator)
		    (car args))
	    (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
	  '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq read-extended-command-predicate
        #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

(electric-pair-mode 1)

(add-hook 'prog-mode-hook (lambda() (add-hook 'before-save-hook 'whitespace-cleanup nil t)))

(setq-default sentence-end-double-space nil)

(global-subword-mode 1)

(setq scroll-conservatively 1000)

(column-number-mode)

(global-display-line-numbers-mode t)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                eshell-mode-hook
		eat-mode-hook
                pdf-view-mode-hook
                doc-view-mode-hook
                nov-mode-hook
		))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(add-hook 'prog-mode-hook 'hs-minor-mode)

(setq-default initial-scratch-message nil)

(defalias 'yes-or-no-p 'y-or-n-p)

(global-auto-revert-mode 1)

(setq undo-limit 67108864 ; 64mb.
      undo-strong-limit 100663296 ; 96mb.
      undo-outer-limit 1006632960) ; 960mb.
(setq auto-save-default t)

(setq window-resize t)

(add-hook 'prog-mode-hook (lambda () (setq truncate-lines t)))

(setq auto-hscroll-mode 'current-line)

(bind-key "M-u" #'upcase-dwim)
(bind-key "M-l" #'downcase-dwim)
(bind-key "M-c" #'capitalize-dwim)

(bind-key "C-x C-f" #'find-file-at-point)

(setq-default proced-auto-update-flag t)
(setq proced-auto-update-interval 1)
(setq-default proced-format 'custom)
(setq proced-goal-attribute nil)
(setq proced-enable-color-flag t)
(with-eval-after-load 'proced
  (add-to-list
   'proced-format-alist
   '(custom user pid ppid sess tree pcpu pmem rss start time state (args comm))))

(setq isearch-lazy-count t)

(setq visible-bell t)

(setq x-stretch-cursor t)

(with-eval-after-load 'mule-util
  (setq truncate-string-ellipsis "…"))

(setq display-time-format "%-d/%-m %-H:%M:%S"
      display-time-interval 1)

(setq frame-title-format
	'(""
	  (:eval
	   (if (string-match-p (regexp-quote (or (bound-and-true-p org-roam-directory) "\u0000"))
			       (or buffer-file-name ""))
	       (replace-regexp-in-string
		".*/[0-9]*-?" "☰ "
		(subst-char-in-string ?_ ?\s buffer-file-name))
	     "%b"))
	  (:eval
	   (when-let ((project-name (and (featurep 'projectile) (projectile-project-name))))
	     (unless (string= "-" project-name)
	       (format (if (buffer-modified-p)  " ◉ %s" "  ●  %s") project-name))))))

(define-prefix-command 'special-insert-map)
(bind-key "M-o" special-insert-map)

(defun vs/suspend-system ()
  "Suspend using systemctl suspend"
  (interactive)
  (shell-command "systemctl suspend"))

(defun ff-link-org ()
  "Gets the current firefox tab link and title, and returns an org link."
  (interactive)
  (setenv "FIREFOX_USER" (shell-command-to-string "grep -B1 'Default=1' ~/.mozilla/firefox/profiles.ini | head -n 1 | cut -d = -f2 | tr -d ' ' | tr -d '\n'"))
  (insert (shell-command-to-string "lz4jsoncat ~/.mozilla/firefox/$FIREFOX_USER/sessionstore-backups/recovery.jsonlz4 | jq -r '.windows[0].tabs | sort_by(.lastAccessed)[-1] | .entries[.index-1] | \"[[\" + (.url) + \"][\" + (.title) + \"]]\"' | tr -d '\n'"))
  )
(with-eval-after-load 'org (bind-key "C-c d l" #'ff-link-org org-mode-map))

;; (require 'package) ;initialize package sources

;; (setq package-archives '(("melpa" . "https://melpa.org/packages/")
;;                          ("org" . "https://orgmode.org/elpa/")
;;                          ("nongnu" . "https://elpa.nongnu.org/nongnu/")
;;                          ("elpa" . "https://elpa.gnu.org/packages/")))

;; (package-initialize)
;; ;;if emacs is not finding packages, use M-x package-refresh-contents and try again
;; (unless package-archive-contents
;;   (package-refresh-contents))

;; 

;; (setq elpaca-core-date '(20240324)) ;; This version of Emacs was built on 2024-01-01
(defvar elpaca-installer-version 0.9)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil :depth 1 :inherit ignore
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let* ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                  ((zerop (apply #'call-process `("git" nil ,buffer t "clone"
                                                  ,@(when-let* ((depth (plist-get order :depth)))
                                                      (list (format "--depth=%d" depth) "--no-single-branch"))
                                                  ,(plist-get order :repo) ,repo))))
                  ((zerop (call-process "git" nil buffer t "checkout"
                                        (or (plist-get order :ref) "--"))))
                  (emacs (concat invocation-directory invocation-name))
                  ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                        "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                  ((require 'elpaca))
                  ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

(require 'elpaca-menu-elpa)
(setf (alist-get 'packages-url (alist-get 'gnu elpaca-menu-elpas))
      "https://git.savannah.gnu.org/gitweb/?p=emacs/elpa.git;a=blob_plain;f=elpa-packages;hb=HEAD"
      (alist-get 'packages-url (alist-get 'nongnu elpaca-menu-elpas))
      "https://git.savannah.gnu.org/gitweb/?p=emacs/nongnu.git;a=blob_plain;f=elpa-packages;hb=HEAD")

;; Uncomment for systems which cannot create symlinks:
;; (elpaca-no-symlink-mode)

;; Install a package via the elpaca macro
;; See the "recipes" section of the manual for more details.

;; (elpaca example-package)

;; Install use-package support
(elpaca elpaca-use-package
  ;; Enable :elpaca use-package keyword.
  (elpaca-use-package-mode)
  ;; Assume :elpaca t unless otherwise specified.
  ;; (setq elpaca-use-package-by-default t)
  (setq use-package-always-ensure t)
  (setq use-package-always-defer t)
  (setq use-package-always-demand nil)
  )

;; Block until current queue processed.
(elpaca-wait)

;;When installing a package which modifies a form used at the top-level
;;(e.g. a package which adds a use-package key word),
;;use `elpaca-wait' to block until that package has been installed/configured.
;;For example:
;;(use-package general :demand t)
;; (elpaca-wait)

(use-package diminish :demand)
(elpaca-wait)

(use-package benchmark-init
  :demand t
  :init (benchmark-init/activate)
  :config (add-hook 'elpaca-after-init-hook 'benchmark-init/deactivate))

(use-package dired
  :ensure nil
  :commands dired
  :hook
  ((dired-mode . dired-hide-details-mode)
   (dired-mode . hl-line-mode)
   (dired-mode . dired-async-mode))
  :custom
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)
  (dired-mouse-drag-files t)                   ; added in Emacs 29
  (mouse-drag-and-drop-region-cross-program t) ; added in Emacs 29
  (dired-dwim-target t "If you have two dired buffers, copy will target the other buffer's directory by default. Press M-n (next-history-element) to return to default behavior")
  (dired-vc-rename-file t "Will rename files using '\vc-rename-file'\ if file is under version control")
  (delete-by-moving-to-trash t))

(use-package dired-x
  :ensure nil
  :config
  ;; Make dired-omit-mode hide all "dotfiles"
  (setq dired-omit-files
	  (concat dired-omit-files "\\|^\\..*$")))

;; Addtional syntax-ppss highlighting for dired
(use-package diredfl
  :hook
  ((dired-mode . diredfl-mode)
   ;; highlight parent and directory preview as well
   (dirvish-directory-view-mode . diredfl-mode))
  :config
  (set-face-attribute 'diredfl-dir-name nil :bold t))

(use-package tramp
  :ensure nil
  :custom
  ;; Tips to speed up connections
  (setq tramp-verbose 0)
  (setq tramp-chunksize 2000)
  (setq tramp-use-ssh-controlmaster-options nil)
  :config
  ;; Enable full-featured Dirvish over TRAMP on certain connections
  ;; https://www.gnu.org/software/tramp/#Improving-performance-of-asynchronous-remote-processes-1.
  (add-to-list 'tramp-connection-properties
		 (list (regexp-quote "/ssh:YOUR_HOSTNAME:")
		       "direct-async-process" t)))

(use-package meow
  :demand t
  :custom
  (meow-keypad-describe-delay 0.1)
  (meow-use-enhanced-selection-effect t)
  (meow-use-clipboard t)
  :config
  (meow-setup-indicator)
  (meow-setup-line-number)
  (meow-define-state disable "Dummy state"
		       :lighter " [D]")

  (add-to-list 'meow-mode-state-list '(vterm-mode . disable))
  ;; (add-to-list 'meow-mode-state-list '(eat-mode . disable))
  ;; (add-to-list 'meow-mode-state-list '(eat-mode . insert))
  (add-to-list 'meow-mode-state-list '(bufler-list-mode . disable))
  (add-to-list 'meow-mode-state-list '(calibredb-show-mode . motion))
  (add-to-list 'meow-mode-state-list '(calibredb-search-mode . motion))

  (meow-thing-register 'angle
			 '(pair ("<") (">"))
			 '(pair ("<") (">")))
  (add-to-list 'meow-char-thing-table
		 '(?a . angle))
  
  (meow-thing-register 'inline-math
			 '(pair ("\\(") ("\\)"))
			 '(pair ("\\(") ("\\)")))
  (add-to-list 'meow-char-thing-table
		 '(?m . inline-math))
  (meow-thing-register 'math
			 '(pair ("\\[") ("\\]"))
			 '(pair ("\\[") ("\\]")))
  (add-to-list 'meow-char-thing-table
		 '(?M . math))

  (defun meow-setup ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
    (meow-motion-overwrite-define-key
     '("j" . meow-next)
     '("k" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     ;; SPC j/k will run the original command in MOTION state.
     '("j" . "H-j")
     '("k" . "H-k")
     ;; Use SPC (0-9) for digit arguments.
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument)
     '("/" . meow-keypad-describe-key)
     '("?" . meow-cheatsheet))
    (meow-normal-define-key
     '("0" . meow-expand-0)
     '("9" . meow-expand-9)
     '("8" . meow-expand-8)
     '("7" . meow-expand-7)
     '("6" . meow-expand-6)
     '("5" . meow-expand-5)
     '("4" . meow-expand-4)
     '("3" . meow-expand-3)
     '("2" . meow-expand-2)
     '("1" . meow-expand-1)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("d" . meow-delete)
     '("D" . meow-backward-delete)
     '("e" . meow-next-word)
     '("E" . meow-next-symbol)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("h" . meow-left)
     '("H" . meow-left-expand)
     '("i" . meow-insert)
     '("I" . meow-open-above)
     '("j" . meow-next)
     '("J" . meow-next-expand)
     '("k" . meow-prev)
     '("K" . meow-prev-expand)
     '("l" . meow-right)
     '("L" . meow-right-expand)
     '("m" . meow-join)
     '("n" . meow-search)
     '("o" . er/expand-region)
     '("O" . meow-to-block)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("Q" . meow-goto-line)
     '("r" . meow-replace)
     '("R" . meow-swap-grab)
     '("s" . meow-kill)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-visit)
     '("w" . meow-mark-word)
     '("W" . meow-mark-symbol)
     '("x" . meow-line)
     '("X" . meow-goto-line)
     '("y" . meow-save)
     '("Y" . meow-sync-grab)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("<escape>" . ignore)))
  (defun meow-setup-colemak ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-colemak-dh)
    (meow-motion-overwrite-define-key
     ;; Use e to move up, n to move down.
     ;; Since special modes usually use n to move down, we only overwrite e here.
     '("e" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     '("?" . meow-cheatsheet)
     ;; To execute the originally e in MOTION state, use SPC e.
     '("e" . "H-e")
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument))
    (meow-normal-define-key
     '("0" . meow-expand-0)
     '("1" . meow-expand-1)
     '("2" . meow-expand-2)
     '("3" . meow-expand-3)
     '("4" . meow-expand-4)
     '("5" . meow-expand-5)
     '("6" . meow-expand-6)
     '("7" . meow-expand-7)
     '("8" . meow-expand-8)
     '("9" . meow-expand-9)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("/" . meow-visit)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("d" . meow-delete)
     '("e" . meow-next)
     '("E" . meow-next-expand)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("h" . meow-block)
     '("H" . meow-to-block)
     '("i" . meow-prev)
     '("I" . meow-prev-expand)
     '("j" . meow-join)
     '("k" . meow-kill)
     '("l" . meow-line)
     '("L" . meow-goto-line)
     '("m" . meow-mark-word)
     '("M" . meow-mark-symbol)
     '("n" . meow-left)
     '("N" . meow-left-expand)
     '("o" . meow-right)
     '("O" . meow-right-expand)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("r" . meow-replace)
     '("s" . meow-insert)
     '("S" . meow-open-above)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-search)
     '("w" . meow-next-word)
     '("W" . meow-next-symbol)
     '("x" . meow-delete)
     '("X" . meow-backward-delete)
     '("y" . meow-save)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("<escape>" . ignore)))
  (meow-setup)
  (meow-global-mode 1)
  )

(use-package expand-region
  :bind ("C-=" . er/expand-region))

(use-package undo-fu
  :bind
  ("M-_" . undo-fu-only-redo)
  ("C-/" . undo-fu-only-undo)
  :config
  (with-eval-after-load 'evil
    (setq evil-undo-system 'undo-fu)))

(use-package undo-fu-session
  :init
  (undo-fu-session-global-mode))

(use-package vundo
  :commands vundo
  :bind ("C-x C-/" . vundo))

;; Example configuration for Consult
(use-package consult
  :bind (;; C-c bindings in `mode-specific-map'
  	 ("C-c M-x" . consult-mode-command)
  	 ("C-c h" . consult-history)
  	 ("C-c k" . consult-kmacro)
  	 ("C-c m" . consult-man)
  	 ("C-c i" . consult-info)
  	 ([remap Info-search] . consult-info)
  	 ;; C-x bindings in `ctl-x-map'
  	 ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
  	 ([remap switch-to-buffer] . consult-buffer)                ;; orig. switch-to-buffer
  	 ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
  	 ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
  	 ("C-x t b" . consult-buffer-other-tab)    ;; orig. switch-to-buffer-other-tab
  	 ([remap bookmark-jump] . consult-bookmark)            ;; orig. bookmark-jump
  	 ([remap project-switch-to-buffer] . consult-project-buffer)      ;; orig. project-switch-to-buffer
  	 ;; Custom M-# bindings for fast register access
  	 ("M-#" . consult-register-load)
  	 ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
  	 ("C-M-#" . consult-register)
  	 ;; Other custom bindings
  	 ([remap yank-pop] . consult-yank-pop)                ;; orig. yank-pop
  	 ;; M-g bindings in `goto-map'
  	 ("M-g e" . consult-compile-error)
  	 ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
  	 ([remap goto-line] . consult-goto-line)             ;; orig. goto-line
  	 ("M-g o" . (lambda() (interactive)(if (eq major-mode 'org-mode)
  					       (consult-org-heading)
  					     (consult-outline))))
  	 ("M-g m" . consult-mark)
  	 ("M-g k" . consult-global-mark)
  	 ("M-g i" . consult-imenu)
  	 ("M-g I" . consult-imenu-multi)
  	 ;; M-s bindings in `search-map'
  	 ("M-s d" . consult-find)                  ;; Alternative: consult-fd
  	 ("M-s c" . consult-locate)
  	 ("M-s g" . consult-grep)
  	 ("M-s G" . consult-git-grep)
  	 ("M-s r" . consult-ripgrep)
  	 ("M-s l" . consult-line)
  	 ("M-s L" . consult-line-multi)
  	 ("M-s k" . consult-keep-lines)
  	 ("M-s u" . consult-focus-lines)
  	 ;; Isearch integration
  	 ("M-s e" . consult-isearch-history)
  	 :map isearch-mode-map
  	 ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
  	 ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
  	 ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
  	 ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
  	 ;; Minibuffer history
  	 :map minibuffer-local-map
  	 ("M-s" . consult-history)                 ;; orig. next-matching-history-element
  	 ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  :custom
  (consult-preview-excluded-buffers '(major-mode . exwm-mode))

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  :init
  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
  	register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
  	xref-show-definitions-function #'consult-xref)

  :config
  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  (setq consult-preview-key 'any)
  ;; (setq consult-preview-key "M-.")
  ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key "M-."
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; "C-+"

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
    ;;;; 1. project.el (the default)
  (setq consult-project-function #'consult--default-project-function)
    ;;;; 2. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
    ;;;; 3. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
    ;;;; 4. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
    ;;;; 5. No project support
  ;; (setq consult-project-function nil)

  (consult-customize
   consult--source-buffer
   :items (lambda ()
            (consult--buffer-query
             :sort 'visibility
             :as #'buffer-name
             :predicate (lambda (buf) (not
				       (or
					(and
					 (featurep 'consult-org-roam)
					 (org-roam-buffer-p buf))
					(and
					 (featurep 'exwm)
					 (eq 'exwm-mode (buffer-local-value 'major-mode buf)))))))))
   
   (setq +consult-source-exwm
	 `(:name      "EXWM"
		      :narrow    ?x
		      ;; :hidden t
		      :category  buffer
		      :face      consult-buffer
		      :history   buffer-name-history
		      ;; Specify either :action or :state
		      :action    ,#'consult--buffer-action ;; No preview
		      ;; :state  ,#'consult--buffer-state  ;; Preview
		      :items
		      ,(lambda () (consult--buffer-query
				   :sort 'visibility
				   :as #'buffer-name
				   ;; :exclude (remq +consult-exwm-filter consult-buffer-filter)
				   :mode 'exwm-mode))))
   ;; "EXWM buffer source."
   (add-to-list 'consult-buffer-sources '+consult-source-exwm 'append)
   )

(use-package consult-dir
  :after vertico
  ;; :custom
  ;; (consult-dir-project-list-function #'consult-dir-projectile-dirs)
  :config
  (add-to-list 'consult-dir-sources 'consult-dir--source-tramp-ssh t)
  :bind (("C-x C-d" . consult-dir)
	   :map vertico-map
	   ("C-x C-d" . consult-dir)
	   ("C-x C-j" . consult-dir-jump-file)))

(use-package consult-project-extra
  :bind
  (("C-x p f" . consult-project-extra-find)
   ("C-x p o" . consult-project-extra-find-other-window))
  :config
  (delete '(project-find-file "Find file") project-switch-commands)
  (add-to-list 'project-switch-commands '(consult-project-extra-find "Find file")))

(use-package recentf
  :ensure nil
  :hook (elpaca-after-init-hook . recentf-mode))

(use-package vertico
  :custom
  (vertico-count 13)                    ; Number of candidates to display
  (vertico-resize t)
  (vertico-cycle nil) ; Go from last to first candidate and first to last (cycle)?
  (read-file-name-completion-ignore-case t)
  (read-buffer-completion-ignore-case t)
  (completion-ignore-case t)
  (vertico-grid-separator "       ")
  (vertico-grid-lookahead 50)
  (vertico-buffer-display-action '(display-buffer-reuse-window)) ; Default
  (vertico-multiform-categories                                  ; Choose a multiform
   '((consult-grep buffer)
     (consult-location)
     (imenu buffer)
     (library reverse indexed)
     (org-roam-node reverse indexed)
     (t reverse)
     ))
  ;; (vertico-multiform-commands
  ;;  '(("flyspell-correct-*" grid reverse)
  ;; 	 (org-refile grid reverse indexed)
  ;; 	 (consult-yank-pop indexed)
  ;; 	 (consult-flycheck)
  ;; 	 (consult-lsp-diagnostics)
  ;; 	 (jinx grid (vertico-grid-annotate . 20))
  ;;  ))

  :bind ((:map vertico-map
		 ;; Option 1: Additional bindings
		 ("?" . minibuffer-completion-help)
		 ("M-RET" . minibuffer-force-complete-and-exit)
		 ("C-M-<return>" . vertico-exit-input)
		 ;; ("M-TAB" . minibuffer-complete)
		 ;; Option 2: Replace `vertico-insert' to enable TAB prefix expansion.
		 ("TAB" . minibuffer-complete)
		 ("C-M-n" . vertico-next-group)
		 ("C-M-p" . vertico-previous-group)

		 ;; For vertico multiform
		 ;; ("C-i" . vertico-quick-insert)
		 ("C-o" . vertico-quick-exit)
		 ("M-G" . vertico-multiform-grid)
		 ("M-F" . vertico-multiform-flat)
		 ("M-R" . vertico-multiform-reverse)
		 ("M-U" . vertico-multiform-unobtrusive)

		 ("<backspace>" . vertico-directory-delete-char)
		 ("C-w" . vertico-directory-delete-word)
		 ("C-<backspace>" . vertico-directory-delete-word)
		 ("RET" . vertico-directory-enter)
		 )
	   (:map global-map
		 ("C-x M-." . vertico-repeat)
		 )
	   )

  :config
  (defun +embark-live-vertico ()
    "Shrink Vertico minibuffer when `embark-live' is active."
    (when-let (win (and (string-prefix-p "*Embark Live" (buffer-name))
			  (active-minibuffer-window)))
	(with-selected-window win
	  (when (and (bound-and-true-p vertico--input)
		     (fboundp 'vertico-multiform-unobtrusive))
	    (vertico-multiform-unobtrusive)))))
  (add-hook 'embark-collect-mode-hook #'+embark-live-vertico)

  ;; Use `consult-completion-in-region' if Vertico is enabled.
  ;; Otherwise use the default `completion--in-region' function.
  ;; (setq completion-in-region-function
  ;; 	(lambda (&rest args)
  ;; 	  (apply (if vertico-mode
  ;; 		     #'consult-completion-in-region
  ;; 		   #'completion--in-region)
  ;; 		 args)))

  ;; (defun vertico-resize--minibuffer ()
  ;;   (add-hook 'window-size-change-functions
  ;; 	      (lambda (win)
  ;; 		(let ((height (window-height win)))
  ;; 		  (when (/= (1- height) vertico-count)
  ;; 		    (setq-local vertico-count (1- height))
  ;; 		    (vertico--exhibit))))
  ;; 	      t t))

  ;; (advice-add #'vertico--setup :before #'vertico-resize--minibuffer)

  ;; (defvar +vertico-transform-functions nil)

  ;; (cl-defmethod vertico--format-candidate :around
  ;;   (cand prefix suffix index start &context ((not +vertico-transform-functions) null))
  ;;   (dolist (fun (ensure-list +vertico-transform-functions))
  ;;     (setq cand (funcall fun cand)))
  ;;   (cl-call-next-method cand prefix suffix index start))

  ;; function to highlight enabled modes similar to counsel-M-x
  ;; (defun +vertico-highlight-enabled-mode (cmd)
  ;;   "If MODE is enabled, highlight it as font-lock-constant-face."
  ;;   (let ((sym (intern cmd)))
  ;;     (if (or (eq sym major-mode)
  ;; 	      (and
  ;; 	       (memq sym minor-mode-list)
  ;; 	       (boundp sym)))
  ;; 	  (propertize cmd 'face 'font-lock-constant-face)
  ;; 	cmd)))

  :init ;; doesn't work with :config
  (vertico-mode)
  (vertico-mouse-mode)
  (vertico-multiform-mode) ;; easy way to change view modes

  :hook
  (minibuffer-setup . vertico-repeat-save) ; Make sure vertico state is saved for `vertico-repeat'
  (rfn-eshadow-update-overlay . vertico-directory-tidy) ; Correct file path when changed

  )

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :ensure nil
  :init (savehist-mode)
  ;; :hook (vertico-mode)
  )

(use-package vertico-posframe
  :after vertico
  :custom
  (vertico-multiform-commands
   '(
     (execute-extended-command posframe
				 (+vertico-transform-functions . +vertico-highlight-enabled-mode))
     ("flyspell-correct-*" (:not posframe) grid reverse)
     (org-refile grid reverse indexed)
     (consult-yank-pop (:not posframe) indexed)
     (consult-flycheck)
     (consult-lsp-diagnostics)
     ("jinx" grid (vertico-grid-annotate . 20))
     (consult-line
	posframe
	(vertico-posframe-poshandler . posframe-poshandler-frame-bottom-center)
	(vertico-posframe-border-width . 10)
	;; NOTE: This is useful when emacs is used in both in X and
	;; terminal, for posframe do not work well in terminal, so
	;; vertico-buffer-mode will be used as fallback at the
	;; moment.
	(vertico-posframe-fallback-mode . vertico-mode))
     (t posframe)
     ))
  ;; (vertico-posframe-parameters
  ;;  '((left-fringe . 40)
  ;;    (right-fring . 40)))
  (vertico-posframe-vertico-multiform-key "M-I")
  ;; :config
  ;; (vertico-posframe-cleanup)
  )

;; Optionally use the `orderless' completion style.
(use-package orderless
  :custom
  (completion-styles '(substring orderless partial-completion basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion))
				   (eglot (styles orderless))
                                   (eglot-capf (styles orderless))))
  (orderless-matching-styles
   '(orderless-literal
     orderless-prefixes
     orderless-initialism
     orderless-regexp
     ;; orderless-flex                       ; Basically fuzzy finding
     ;; orderless-strict-leading-initialism
     ;; orderless-strict-initialism
     ;; orderless-strict-full-initialism
     ;; orderless-without-literal          ; Recommended for dispatches instead
     )))

(use-package marginalia
  :bind (:map minibuffer-local-map
		("M-A" . marginalia-cycle))
  :custom
  (marginalia-max-relative-age 0)
  (marginalia-align 'right)
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init ;; doesn't work with :config
  (marginalia-mode))

;; Add extensions
(use-package cape
  :demand
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  ;; :bind (("C-c c p" . completion-at-point) ;; capf
  ;;        ("C-c c t" . complete-tag)        ;; etags
  ;;        ("C-c c d" . cape-dabbrev)        ;; or dabbrev-completion
  ;;        ("C-c c h" . cape-history)
  ;;        ("C-c c f" . cape-file)
  ;;        ("C-c c k" . cape-keyword)
  ;;        ("C-c c s" . cape-elisp-symbol)
  ;;        ("C-c c e" . cape-elisp-block)
  ;;        ("C-c c a" . cape-abbrev)
  ;;        ("C-c c l" . cape-line)
  ;;        ("C-c c w" . cape-dict)
  ;;        ("C-c c :" . cape-emoji)
  ;;        ("C-c c \\" . cape-tex)
  ;;        ("C-c c _" . cape-tex)
  ;;        ("C-c c ^" . cape-tex)
  ;;        ("C-c c &" . cape-sgml)
  ;;        ("C-c c r" . cape-rfc1345))
  :custom
  (cape-dict-file (list
    		   (expand-file-name "dicts/English.dic" user-emacs-directory)
    		   (expand-file-name "dicts/Spanish.dic" user-emacs-directory)))
  (cape-dabbrev-min-length 2)
  :config
  ;; For pcomplete. For now these two advices are strongly recommended to
  ;; achieve a sane Eshell experience. See
  ;; https://github.com/minad/corfu#completing-with-corfu-in-the-shell-or-eshell
  (with-eval-after-load 'eglot
    (setq completion-category-defaults nil)
    (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster))

  (defun my/eglot-capf ()
    (setq-local completion-at-point-functions
		(list (cape-capf-super
                       #'eglot-completion-at-point
                       #'tempel-expand
                       #'cape-file))))

  (add-hook 'eglot-managed-mode-hook #'my/eglot-capf)
  :init
  ;; (require 'cape)
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  ;; as `add-to-list' adds at the start, the last function added is the first
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-dict)
  ;; (add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-file)
  ;; (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;; (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  ;;(add-to-list 'completion-at-point-functions #'cape-history)
  ;; (add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;; (add-to-list 'completion-at-point-functions #'cape-elisp-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
  ;; :init

  (defun kb/cape-capf-setup-lsp ()
    "Replace the default `lsp-completion-at-point' with its
      `cape-capf-buster' version. Also add `cape-file' and
      `cape-dabbrev' backends."
    (setf (elt (cl-member 'lsp-completion-at-point completion-at-point-functions) 0)
    	  (cape-capf-buster #'lsp-completion-at-point))
    ;; (add-to-list 'completion-at-point-functions (cape-company-to-capf #'company-yasnippet))
    (add-to-list 'completion-at-point-functions #'tempel-complete)
    (add-to-list 'completion-at-point-functions #'cape-file t)
    (add-to-list 'completion-at-point-functions #'cape-dabbrev t)
    )

  (defun cape/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
    	  '(orderless))
    ;; Optionally configure the first word as flex filtered.
    ;; (add-hook 'orderless-style-dispatchers #'my/orderless-dispatch-flex-first nil 'local)
    ;; Optionally configure the cape-capf-buster.
    (setq-local completion-at-point-functions
    		(list
    		 (cape-capf-buster #'lsp-completion-at-point)
    		 (tempel-complete)
    		 (cape-file)
    		 (cape-dabbrev))))

  (defun kb/cape-capf-setup-org ()
    (require 'org-roam)
    (add-to-list 'completion-at-point-functions #'cape-dabbrev)
    (add-to-list 'completion-at-point-functions #'cape-elisp-block)
    (add-to-list 'completion-at-point-functions #'tempel-complete)
    (if (org-roam-file-p)
    	(org-roam--register-completion-functions-h))
    )

  (defun kb/cape-capf-setup-latex ()
    "Add LaTeX-specific completions to `completion-at-point-functions'."
    (add-to-list 'completion-at-point-functions #'cape-dabbrev)
    (add-to-list 'completion-at-point-functions #'cape-tex)
    (add-to-list 'completion-at-point-functions #'tempel-complete)
    ;; (add-to-list 'completion-at-point-functions #'cape-dict)
    )
  :hook
  (org-mode . kb/cape-capf-setup-org)
  (org-roam-mode . kb/cape-capf-setup-org)
  (lsp-mode . kb/cape-capf-setup-lsp)
  (LaTeX-mode . kb/cape-capf-setup-latex)
  (latex-mode . kb/cape-capf-setup-latex)
  )

(use-package embark
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :custom
  ;; Optionally replace the key help with a completing-read interface
  (prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
		 '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
		   nil
		   (window-parameters (mode-line-format . none))))

  ;; :init

  ;; Show the Embark target at point via Eldoc. You may adjust the
  ;; Eldoc strategy, if you want to see the documentation from
  ;; multiple providers. Beware that using this can be a little
  ;; jarring since the message shown in the minibuffer can be more
  ;; than one line, causing the modeline to move up and down:

  ;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)
  )

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :after (embark consult)
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package embark-vc
  :after (embark magit))

(use-package all-the-icons)

(use-package doom-themes
  :demand
  :disabled
  :config
  (remove-hook 'doom-load-theme-hook #'doom-themes-org-config)
  (load-theme 'doom-one t)
  ;; Enable flashing mode-line on errors
  ;; (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  ;; (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  ;; (doom-themes-org-config)
  )

(use-package modus-themes
  :demand
  :hook (after-init-hook . (lambda () (load-theme 'modus-vivendi :noconfirm)))
  :custom
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs t)
  (modus-themes-mixed-fonts t)
  (modus-themes-variable-pitch-ui nil)
  (modus-themes-custom-auto-reload t)
  (modus-themes-disable-other-themes t)

  (modus-themes-prompts '(bold))
  (modus-themes-completions
   '((matches . (extrabold underline))
     (selection . (semibold text-also))))

  (modus-themes-headings
   '((1 . (1.5))
     (2 . (1.3))
     (agenda-date . (1.3))
     (agenda-structure . (variable-pitch light 1.8))
     (t . (1.1))))

  :config
  (setq modus-themes-common-palette-overrides
   `(
     ;; Modeline without borders
     (border-mode-line-active unspecified)
     (border-mode-line-inactive unspecified)

     ;; (fg-line-number-inactive fg-dim)
     ;; (bg-line-number-inactive unspecified)

     ;; (fg-line-number-active fg-main)
     ;; (bg-line-number-active bg-lavender)

     ;; (fringe unspecified)

     ;; (cursor blue-intense)

     ;; (fg-prompt cyan-faint)
     ;; (bg-prompt unspecified)

     (bg-region bg-lavender)
     (fg-region unspecified)

     ;; ;; For vertico and related
     ;; (fg-completion-match-0 fg-main)
     ;; (fg-completion-match-1 fg-main)
     ;; (fg-completion-match-2 fg-main)
     ;; (fg-completion-match-3 fg-main)
     ;; (bg-completion-match-0 bg-blue-subtle)
     ;; (bg-completion-match-1 bg-magenta-subtle)
     ;; (bg-completion-match-2 bg-cyan-subtle)
     ;; (bg-completion-match-3 bg-red-subtle)
     

     ;; ;; Everything with "prose" is mainly for org
     ;; (bg-prose-block-contents bg-dim)
     ;; (bg-prose-block-delimiter bg-mode-line-inactive)
     ;; (fg-prose-block-delimiter fg-main)

     (prose-done fg-dim)
     (prose-todo red)

     (fg-heading-1 blue)
     ;; ;; (bg-heading-1 bg-dim)
     ;; ;; (overline-heading-1 border)
     (fg-heading-2 cyan)
     (fg-heading-3 magenta)

     (date-common cyan)   ; default value (for timestamps and more)
     (date-deadline red-warmer)
     (date-event magenta-warmer)
     (date-holiday blue) ; for M-x calendar
     (date-now yellow-warmer)
     (date-scheduled magenta-cooler)
     (date-weekday cyan-cooler)
     (date-weekend blue-faint)
     
     (bg-prose-code bg-green-nuanced)
     (fg-prose-code green-cooler)

     (bg-prose-verbatim bg-magenta-nuanced)
     (fg-prose-verbatim magenta-warmer)

     (bg-prose-macro bg-blue-nuanced)
     (fg-prose-macro magenta-cooler)

     (bg-button-active bg-main)
     (fg-button-active fg-main)
     (bg-button-inactive bg-inactive)
     (fg-button-inactive "gray50")

     ;; (highlight yellow-intense)

     ;; ;; (bg-hover t)

     (string green-warmer)
     (builtin red-faint)
     (docstring fg-alt)
     (docmarkup green)
     (fnname magenta-warmer)
     (keyword blue-faint)
     (constant magenta)
     (preprocessor red-faint)
     (type cyan-cooler)
     (variable blue)

     (bg-paren-match red-intense)

     ,@modus-themes-preset-overrides-faint
     ))
  (load-theme 'modus-vivendi :noconfirm))

(use-package nerd-icons)

(use-package nerd-icons-dired
  ;; :after (nerd-icons)
  :hook
  (dired-mode . nerd-icons-dired-mode))

(use-package nerd-icons-completion
  :after marginalia
  :init
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package nerd-icons-corfu
  :after corfu
  :init
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package moody
  :demand
  :hook (elpaca-after-init-hook . moody-replace-mode-line-buffer-identification)
  :custom
  (x-underline-at-descent-line t)
  (moody-mode-line-height 34)
  :config
  ;; (let ((line (face-attribute 'mode-line :underline)))
    ;; (set-face-attribute 'mode-line          nil :overline   line)
    ;;   (set-face-attribute 'mode-line          nil :box        line)
    ;;   (set-face-attribute 'mode-line-inactive nil :overline   nil)
    ;;   (set-face-attribute 'mode-line-inactive nil :underline  "#484337")
    ;;   (set-face-attribute 'mode-line-inactive nil :box        nil)
    ;;   (set-face-attribute 'mode-line-inactive nil :background "#282C34")
    ;;   (set-face-attribute 'mode-line-inactive nil :foreground nil)
    ;; )
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (moody-replace-eldoc-minibuffer-message-function)
  )

(use-package minions
  :demand
  :config (minions-mode))

(use-package which-key
  :demand
  :diminish
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.2))

(use-package helpful
  :bind
  ([remap describe-function] . helpful-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-symbol] . helpful-symbol)
  ([remap describe-key] . helpful-key))

(use-package free-keys)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package ace-window
  :init (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
		aw-char-position 'left
		aw-ignore-current t
		aw-leading-char-style 'char
		aw-scope 'frame)
  :bind (("C-x o" . ace-window))
  :config
  (ace-window-posframe-mode 1)
  )

(use-package bufler
  :bind ("C-x C-b" . bufler-list)
  :config (bufler-mode 1))

(use-package golden-ratio
  :config
  (add-to-list 'golden-ratio-extra-commands 'ace-window)
  (add-to-list 'golden-ratio-extra-commands 'mouse-drag-region))

(use-package lemon
  ;; :demand t
  :defer 2
  :ensure (:fetcher git :url "https://codeberg.org/emacs-weirdware/lemon.git")
  :custom-face
  (lemon-time-face ((nil :inherit bold :foreground unspecified)))
  :custom
  (lemon-sparkline-use-xpm t)
  (lemon-delay 1)
  (lemon-monitors (list '((lemon-time :display-opts '(:format "%H:%M %f | %a %d %b"))
			    (let ((battery-str (battery)))
			      (unless (or (equal "Battery status not available" battery-str)
					  (string-match-p (regexp-quote "N/A") battery-str))
				(lemon-battery)))
			    (lemon-cpu-linux)
			    (lemon-memory-linux)
			    (lemon-linux-network-rx)
			    (lemon-linux-network-tx)
			    )))
  :config
  (lemon-mode 1)
  (lemon-display))

(use-package hyperbole
  ;; :demand
  :disabled
  :defer 10
  :diminish
  :commands hyperbole-mode
  :bind ("C-h h" . hyperbole)
  :config (hyperbole-mode))

(use-package wgrep
  :commands wgrep-change-to-wgrep-mode)

(use-package recomplete
  :commands recomplete-case-style-symbol
  :bind (:map special-insert-map
		("c" . recomplete-case-style-symbol)))

(use-package cycle-at-point
  :bind (:map special-insert-map
		("j" . cycle-at-point)))

(use-package smartparens
  :disabled
  :bind (:map smartparens-mode-map
		("C-M-a" . sp-beginning-of-sexp)
		("C-M-e" . sp-end-of-sexp)
		("C-M-f" . sp-forward-sexp)
		("C-M-b" . sp-backward-sexp)
		("C-M-p" . sp-next-sexp)
		("C-M-n" . sp-previous-sexp))
  :config (require 'smartparens-config))

(use-package ialign
  :bind ("C-x l" . ialign))

(use-package avy
  :bind ("M-j" . avy-goto-char-timer)
  :custom
  (avy-background t))

(use-package latex
  :ensure nil
  :hook
  (latex-mode . LaTeX-mode) ;; absurd that this needs to be added 
  (LaTeX-mode . TeX-fold-mode)
  (LaTeX-mode . turn-on-reftex)
  (LaTeX-mode . LaTeX-math-mode)
  (LaTeX-mode . outline-minor-mode)
  ;; (LaTeX-mode . abbrev-mode)
  (LaTeX-mode . visual-line-mode)
  (LateX-mode . prettify-symbols-mode)
  :bind
  (:map LaTeX-mode-map
	("s-a" . abbrev-mode)
	("s-c" . preview-clearout-at-point)
	("s-q" . LaTeX-fill-buffer)
	;; ("C-c C-l" . latex-math-from-calc)
	)
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (preview-auto-cache-preamble nil)
  (TeX-command-extra-options "-shell-escape")
  ;; Use pdf-tools to open PDF files
  (TeX-view-program-selection '((output-pdf "PDF Tools")))
  (TeX-save-query nil)
  (TeX-show-compilation nil)
  (TeX-engine 'luatex)
  (reftex-plug-into-AUCTeX t)
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-start-server t)
  :config
  (add-to-list 'TeX-command-list '("Make" "make" TeX-run-command nil t))
  ;; Update PDF buffers after successful LaTeX runs
  (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
  )

(use-package auctex
  :demand
  :ensure  (auctex :repo "https://git.savannah.gnu.org/git/auctex.git" :branch "main"
		   :pre-build (("make" "elpa"))
		   :build (:not elpaca--compile-info) ;; Make will take care of this step
		   :files ("*.el" "doc/*.info*" "etc" "images" "latex" "style")
		   :version (lambda (_) (require 'tex-site) AUCTeX-version))
  :mode (("\\.tex\\'" . LaTeX-mode)
	 ("\\.tex\\.erb\\'" . LaTeX-mode)
	 ("\\.etx\\'" . LaTeX-mode))
  :config
  (with-eval-after-load 'preview
    (dolist (env '("tikzpicture" "circuitikz"))
      (add-to-list 'preview-default-preamble
		   (concat "\\PreviewEnvironment{" env "}") t))
    (add-to-list 'preview-inner-environments "axis")
    (set-face-attribute 'preview-reference-face nil :foreground "black" :background "white")
    ))

(use-package cdlatex
  :hook (latex-mode . cdlatex-mode)
  (LaTeX-mode . cdlatex-mode)
  (org-mode . org-cdlatex-mode)
  :custom
  (cdlatex-takeover-parenthesis nil)
  :config
  (add-hook 'cdlatex-tab-hook
    (defun cdlatex-indent-maybe ()
      (when (eq major-mode 'LaTeX-mode)
        (when (or (bolp) (looking-back "^[ \t]+"))
          (LaTeX-indent-line)))))
  ;; (add-hook 'cdlatex-tab-hook
  ;; 	    #'LaTeX-indent-line)
  )

(use-package math-delimiters
  :ensure (math-delimiters :host github :repo "oantolin/math-delimiters")
  :init
  (with-eval-after-load 'latex            ; for AUCTeX
    (define-key LaTeX-mode-map "$" #'math-delimiters-insert))
  
  (with-eval-after-load 'tex-mode         ; for the built-in LaTeX mode
    (define-key latex-mode-map "$" #'math-delimiters-insert))
  
  (with-eval-after-load 'org
    (define-key org-mode-map "$" #'math-delimiters-insert))
  
  (with-eval-after-load 'cdlatex
    (define-key cdlatex-mode-map "$" nil)))

(use-package typst-ts-mode
  :ensure (:type git :host codeberg :repo "meow_king/typst-ts-mode"
                 :files (:defaults "*.el"))
  :mode "\\.typst\\'"
  :custom
  (typst-ts-mode-enable-raw-blocks-highlight t))

(use-package ripgrep)
(use-package ag)

(use-package treesit
  :ensure nil
  :custom
  (treesit-language-source-alist
   ''((astro "https://github.com/virchau13/tree-sitter-astro")
      (css "https://github.com/tree-sitter/tree-sitter-css")
      (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src"))))

(defun efs/lsp-mode-setup ()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode)
  ) ;;upper line with the project folder

(defun corfu-lsp-setup ()
  (setq-local completion-styles '(orderless)
		completion-category-defaults nil))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :custom
  (lsp-completion-provider :none)
  (lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  (lsp-enable-which-key-integration t)
  (lsp-use-plists t)
  :config
  (defun my/orderless-dispatch-flex-first (_pattern index _total)
    (and (eq index 0) 'orderless-flex))

  (defun my/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
	    '(orderless))
    ;; Optionally configured-language the first word as flex filtered.
    (add-hook 'orderless-style-dispatchers #'my/orderless-dispatch-flex-first nil 'local)
    ;; Optionally configure the cape-capf-buster.
    (setq-local completion-at-point-functions (list (cape-capf-buster #'lsp-completion-at-point))))
  :hook (lsp-mode . efs/lsp-mode-setup)
  (lsp-mode . corfu-lsp-setup))


(advice-add 'json-parse-buffer :around
	      (lambda (orig &rest rest)
		(while (re-search-forward "\\u0000" nil t)
		  (replace-match ""))
		(apply orig rest)))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :config
  (setq lsp-ui-doc-position 'bottom))

(use-package consult-lsp
  :after lsp
  :config
  (define-key lsp-mode-map [remap xref-find-apropos] #'consult-lsp-symbols) ;; Replaces xref-find-apropos with lsp symbols
  ;; Defaults `C-M-.' or `C-c l g a'
  )

(use-package lsp-treemacs
  :disabled
  :after lsp)

(use-package typescript-mode
  :mode "\\.ts\\'"
  :hook (typescript-mode . lsp-deferred)
  :config
  (setq typescript-indent-level 2))

(use-package lsp-pyright
  ;; :ensure t
  :hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp))))  ; or lsp-deferred

(use-package pyvenv)

(add-hook 'c-initialization-hook (lambda () (bind-key "RET" #'c-context-line-break 'c-mode-base-map)))
(add-hook 'c-initialization-hook (lambda () (global-cwarn-mode 1)))

(defun vs/c-mode-setup ()
  ;; (electric-pair-mode 1)       ;; Emacs 24
  (electric-pair-local-mode 1) ;; Emacs 25
  )
(add-hook 'c-mode-common-hook 'vs/c-mode-setup)

(setq c-default-style '((java-mode . "java")
			(awk-mode . "awk")
			(other . "linux")))

(defun vs/c++-mode-setup ()
  ;; enable the stuff you want for C# here
  (electric-pair-mode 1)       ;; Emacs 24
  (electric-pair-local-mode 1) ;; Emacs 25
  )
(add-hook 'c++-mode-common-hook 'vs/c++-mode-setup)

(defun my-csharp-mode-hook ()
  ;; enable the stuff you want for C# here
  (electric-pair-mode 1)       ;; Emacs 24
  (electric-pair-local-mode 1) ;; Emacs 25
  )
(add-hook 'csharp-mode-hook 'my-csharp-mode-hook)

(use-package lsp-java)

(use-package lsp-latex
  :after (tex lsp-mode))

(use-package gdscript-mode)

(use-package lua-mode)

(use-package rust-mode
  :config
  (add-hook 'rust-mode-hook
	      (lambda () (setq indent-tabs-mode nil))))
(use-package rustic
  :mode ("\\.rs\\'" . rustic-mode)
  :interpreter ("rust" . rustic-mode)
  :bind (:map rust-mode-map
	      ("C-c C-l" . rust-toggle-mutability)))

(use-package cmake-mode)

(use-package nix-mode
  :mode ("\\.nix\\'" . nix-mode))

(use-package astro-ts-mode
  :disabled
  :mode "\\.astro"
  :init
  (unless (treesit-language-available-p 'astro)
    (mapc #'treesit-install-language-grammar '(astro css tsx))))

(use-package web-mode
  :mode
  (("\\.phtml\\'" . web-mode)
   ("\\.php\\'" . web-mode)
   ("\\.tpl\\'" . web-mode)
   ("\\.[agj]sp\\'" . web-mode)
   ("\\.as[cp]x\\'" . web-mode)
   ("\\.erb\\'" . web-mode)
   ("\\.mustache\\'" . web-mode)
   ("\\.djhtml\\'" . web-mode)
   ("\\.astro\\'" . web-mode))
  :custom
  (web-mode-enable-auto-pairing t)
  (web-mode-enable-auto-expanding t)
  (web-mode-enable-css-coloring t)
  (web-mode-enable-block-face nil)
  (web-mode-enagle-part-face nil)
  (web-mode-enable-interpolation nil)
  (web-mode-enable-current-element-highlight t)
  (web-mode-enable-current-column-highlight t))

(defun corfu-insert-shell-filter (&optional _)
  "Insert completion candidate and send when inside comint/eshell."
  (when (or (derived-mode-p 'eshell-mode) (derived-mode-p 'comint-mode))
    (lambda ()
      (interactive)
      (corfu-insert)
      ;; `corfu-send-shell' was defined above
      (corfu-send-shell))))

(use-package corfu
  :demand
  :diminish
  ;; Optional customizations
  :custom
  (corfu-cycle nil)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto t)                 ;; Enable auto completion
  (corfu-auto-prefix 2)
  (corfu-auto-delay 0.2)

  (corfu-min-width 30)
  ;; (corfu-max-width corfu-min-width)       ; Always have the same width
  (corfu-count 10)
  (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  (corfu-scroll-margin 10)        ;; Use scroll margin

  (corfu-popupinfo-delay '(0.5 . 0.0))
  ;; recommended: Enable Corfu globally.  This is recommended since Dabbrev can
  ;; be used globally (M-/).  See also the customization variable
  ;; `global-corfu-modes' to exclude certain modes.
  :bind
  (:map corfu-map
        ("C-j" . corfu-quick-jump)
        ("C-k" . corfu-popupinfo-toggle)
        ("C-l" . corfu-insert-separator)
        ;; Opt 1: Unbind RET
        ("RET" . nil)
        ;; Opt 2: Use RET only in shell modes
        ;; ("RET" . (menu-item "" nil :filter corfu-insert-shell-filter))
        )
  :config
  (global-corfu-mode)
  (corfu-echo-mode)
  (corfu-history-mode)
  (corfu-popupinfo-mode)
  :hook
  (minibuffer-setup-hook . corfu-enable-always-in-minibuffer)
  :init
  ;; Enable Corfu more generally for every minibuffer, as long as no other
  ;; completion UI is active. If you use Mct or Vertico as your main minibuffer
  ;; completion UI. From
  ;; https://github.com/minad/corfu#completing-with-corfu-in-the-minibuffer
  (defun corfu-enable-always-in-minibuffer ()
    "Enable Corfu in the minibuffer if Vertico/Mct are not active."
    (unless (or (bound-and-true-p mct--active)
                (bound-and-true-p vertico--input)
                (eq (current-local-map) read-passwd-map))
      ;; (setq-local corfu-auto nil) ;; Enable/disable auto completion
      (setq-local corfu-echo-delay nil ;; Disable automatic echo and popup
                  corfu-popupinfo-delay nil)
      (corfu-mode 1)))
  (add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1)
  )

(use-package corfu-terminal
  :after corfu
  :unless (display-graphic-p)
  :config
  (corfu-terminal-mode +1))

;; Configure Tempel
(use-package tempel

  :bind (;;("M-+" . tempel-complete) ;; Alternative tempel-expand
	   ("M-*" . tempel-insert))

  :custom
  (tempel-path (expand-file-name "templates" user-emacs-directory))
  ;; Require trigger prefix before template name when completing.
  ;; (tempel-trigger-prefix "<")

  :init
  ;; Setup completion at point
  (defun tempel-setup-capf ()
    (interactive)
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (add-to-list 'completion-at-point-functions #'tempel-complete))

  ;; (add-hook 'conf-mode-hook 'tempel-setup-capf)
  ;; (add-hook 'prog-mode-hook 'tempel-setup-capf)
  ;; (add-hook 'text-mode-hook 'tempel-setup-capf)
  ;; (add-hook 'lsp-mode-hook 'tempel-setup-capf)

  ;; Optionally make the Tempel templates available to Abbrev,
  ;; either locally or globally. `expand-abbrev' is bound to C-x '.
  ;; (add-hook 'prog-mode-hook #'tempel-abbrev-mode)
  (global-tempel-abbrev-mode)
  )

(use-package tempel-collection
  :after tempel)

(use-package projectile
  :disabled
  :defer 2
  :diminish
  ;; :bind-keymap ("C-c p" . projectile-command-map)
  :bind (:map projectile-mode-map
	   ("C-c p" . projectile-command-map))
  :custom (projectile-project-search-path '("~/projects"))
  ;; (projectile-keymap-prefix "C-c p")
  :config (projectile-mode))
;; NOTE: set this to the folder where you keep your Git repos
;; (when (file-directory-p "~/projects/code")
;; (setq projectile-project-search-path '("~/projects/code")))
;; (setq projectile-switch-project-action #'projectile-dired))
					  ; Review dir locals, 4th video ~minute 18

(use-package consult-projectile
  :requires projectile
  ;; :after projectile
  :bind (
	   ([remap projectile-switch-project] . consult-projectile-switch-project)
	   ([remap projectile-recentf] . consult-projectile-recentf)
	   ([remap projectile-find-dir] . consult-projectile-find-dir)
	   ([remap projectile-switch-to-buffer-other-window] . consult-projectile-switch-to-buffer-other-window)
	   ([remap projectile-switch-to-buffer] . consult-projectile-switch-to-buffer)
	   ([remap projectile-find-file-other-frame] . consult-projectile-find-file-other-frame)
	   ([remap projectile-find-file-other-window] . consult-projectile-find-file-other-window)
	   ([remap projectile-switch-to-buffer-other-frame] . consult-projectile-switch-to-buffer-other-frame)
	   ([remap projectile-find-file] . consult-projectile-find-file)
	   :map projectile-command-map
	   (";" . consult-projectile)
	   )
  )

(defun +elpaca-unload-seq (e)
  (and (featurep 'seq) (unload-feature 'seq t))
  (elpaca--continue-build e))

;; You could embed this code directly in the reicpe, I just abstracted it into a function.
(defun +elpaca-seq-build-steps ()
  (append (butlast (if (file-exists-p (expand-file-name "seq" elpaca-builds-directory))
			 elpaca--pre-built-steps elpaca-build-steps))
	    (list '+elpaca-unload-seq 'elpaca--activate-package)))

;; (use-package seq :ensure `(seq :build ,(+elpaca-seq-build-steps)))
(use-package transient)
(use-package magit
  :commands magit
  :bind ("C-x g" . magit))
;; :custom
;; (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package hl-todo
  :bind ("C-c C-;" . hl-todo-insert)
  :init (global-hl-todo-mode t))

(use-package magit-todos
  :after (magit hl-todo)
  :init (magit-todos-mode t)
  )

(use-package treemacs
  ;; :ensure t
  :defer t
  :bind
  (:map global-map
	  ;; ("M-0"       . treemacs-select-window)
	  ("C-x t 1"   . treemacs-delete-other-windows)
	  ("C-x t t"   . treemacs)
	  ("C-x t d"   . treemacs-select-directory)
	  ("C-x t B"   . treemacs-bookmark)
	  ("C-x t C-t" . treemacs-find-file)
	  ("C-x t M-t" . treemacs-find-tag)))

(use-package flycheck
  :defer 2
  :init (global-flycheck-mode))

(use-package plantuml-mode
    :mode ("\\.plantuml'" . plantuml-mode)
    :custom
    (plantuml-default-exec-mode 'executable)
     :config
     (defun vs/plantuml-capf ()
       "Function used for `completion-at-point-functions' in `plantuml-mode'."
       (let ((completion-ignore-case t) ; Not working for company-capf.
             (bounds (bounds-of-thing-at-point 'symbol))
             (keywords plantuml-kwdList))
         (when (and bounds keywords)
           (list (car bounds)
                 (cdr bounds)
                 keywords
                 :exclusve 'no
                 :company-docsig #'identity))))
    (add-hook 'plantuml-mode #'(add-to-list 'completion-at-point #'vs/plantuml-capf))
)

(use-package envrc
  :commands (envrc-mode envrc-global-mode))

(use-package ediff
  :ensure nil				; It's and integrated package
  :custom
  (ediff-window-setup-function 'ediff-setup-windows-plain "Don't use weird multi-frame setup")
  (ediff-split-window-function 'split-window-horizontally))

(defun ar/org-insert-link-dwim ()
  "Like ` org-insert-link ' but with personal dwim preferences."
  (interactive)
  (let* ((point-in-link (org-in-regexp org-link-any-re 1))
	 (clipboard-url (when (string-match-p  "^http" (current-kill 0))
                          (current-kill 0)))
	 (region-content (when (region-active-p)
                           (buffer-substring-no-properties (region-beginning)
                                                           (region-end)))))
    (cond ((and region-content clipboard-url (not point-in-link))
           (delete-region (region-beginning) (region-end))
           (insert (org-make-link-string clipboard-url region-content)))
          ((and clipboard-url (not point-in-link))
           (insert (org-make-link-string
                    clipboard-url
                    (read-string  "title: "
				  (with-current-buffer (url-retrieve-synchronously clipboard-url)
                                    (dom-text (car
                                               (dom-by-tag (libxml-parse-html-region
                                                            (point-min)
                                                            (point-max))
                                                           'title))))))))
          (t
           (call-interactively 'org-insert-link)))))

(defun vs/org-mode-setup ()
  ;; (org-indent-mode) ;correctly indents org
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (set-fill-column 120))

(use-package org
  ;; :ensure nil
  :ensure `(org
            :remotes ("tecosaur"
                      :repo "https://git.tecosaur.net/tec/org-mode.git"
                      :branch "dev")
            :files (:defaults ("etc/styles/" "etc/styles/*" "doc/*.texi"))
            :build t
            :pre-build
            (progn
              (with-temp-file "org-version.el"
		(require 'lisp-mnt)
		(let ((version
                       (with-temp-buffer
			 (insert-file-contents "lisp/org.el")
			 (lm-header "version")))
                      (git-version
                       (string-trim
			(with-temp-buffer
			  (call-process "git" nil t nil "rev-parse" "--short" "HEAD")
			  (buffer-string)))))
		  (insert
		   (format "(defun org-release () \"The release version of Org.\" %S)\n" version)
		   (format "(defun org-git-version () \"The truncate git commit hash of Org mode.\" %S)\n" git-version)
		   "(provide 'org-version)\n")))
              (require 'elpaca-menu-org)
              (elpaca-menu-org--build))
            :pin nil)
  
:bind
("C-c a" . org-agenda)
("C-c c" . org-capture)
(:map org-mode-map
      ("C-c C-x l" . org-toggle-link-display)
	("C-c C-l" . ar/org-insert-link-dwim))

:custom
(org-directory "~/Documents/org")
(org-log-done 'time "Inserts the time when the thing was setted as DONE")
(org-auto-align-tags t)
(org-special-ctrl-a/e t)
;; (org-insert-heading-respect-content nil) #  REVIEW What does this do?
(org-hide-emphasis-markers t "doesn't show * when bold: *text*")
(org-pretty-entities t)
(org-startup-with-inline-images t "Initialize org with images enabled")
(org-ellipsis " ▾") ;; "…" shows this symbol when a header is "collapsed"

(org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "HOLD(h)" "|" "DONE(d)")))

	;;; Org agenda
;; (org-agenda-start-with-log-mode t)
(org-log-into-drawer t "Inserts state changes into a drawer")
(org-agenda-files (mapcar 'file-truename (file-expand-wildcards "~/Documents/org/*.org")))

;; (org-columns-default-format "%50ITEM(Task) %10CLOCKSUM %16TIMESTAMP_IA" "Agenda column with clock and timestamp, use org-agenda-columns \"C-c C-x C-c\" to show")
;; (org-habit-graph-column 60)

(org-refile-targets '(
		      (org-agenda-files :tag . "project")))
(org-refile-use-outline-path nil) ;; or 'file
(org-outline-path-complete-in-steps nil)

;; (org-agenda-custom-commands
;;  '(
;;    ;; Low-effort next actions
;;    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
;;     ((org-agenda-overriding-header "Low Effort Tasks")
;;      (org-agenda-max-todos 20)
;;      (org-agenda-files org-agenda-files)))
(org-agenda-custom-commands
 '(("g" "Get Things Done (GTD)"
    ((agenda ""
	     ((org-agenda-skip-function
	       '(org-agenda-skip-entry-if 'deadline))
	      (org-deadline-warning-days 0)))
     (todo "NEXT"
	   ((org-agenda-skip-function
	     '(org-agenda-skip-entry-if 'deadline))
	    (org-agenda-prefix-format "  %i %-12:c [%e] ")
	    (org-agenda-overriding-header "\nTasks\n")))
     (agenda nil
	     ((org-agenda-entry-types '(:deadline))
	      (org-agenda-format-date "")
	      (org-deadline-warning-days 7)
	      ;; (org-agenda-skip-function
	      ;;  '(org-agenda-skip-entry-if 'notregexp "\\* NEXT"))
	      (org-agenda-overriding-header "\nDeadlines")))
     (tags-todo "inbox"
		((org-agenda-prefix-format "  %?-12t% s")
		 (org-agenda-overriding-header "\nInbox\n")))
     (tags "CLOSED>=\"<today>\""
	   ((org-agenda-overriding-header "\nCompleted today\n")))))
   ("n" todo "TODO" ((org-agenda-skip-function 'vs/skip-habit-category)
		     (org-agenda-overriding-header "hola")))
   ;; Low-effort next actions
   ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
    ((org-agenda-overriding-header "Low Effort Tasks")
     (org-agenda-max-todos 20)
     (org-agenda-files org-agenda-files)))))

(org-agenda-prefix-format
 '((agenda . " %i %-12:c%?-12t% s")
   (todo   . " %i %-12:c")
   (tags   . " %i %-12:c")
   (search . " %i %-12:c")))

;; ;; Org-capture
(org-capture-templates
 `(("i" "Inbox" entry (file "inbox.org")
    ,(concat "* TODO %?\n"
	     "/Entered on/ %U"))

   ("n" "Note" entry  (file "notes.org")
    ,(concat "* Note (%a)\n"
	     "/Entered on/ %U\n" "\n" "%?"))))
:config
(plist-put org-format-latex-options :scale 2.0) ;; LaTeX options

(add-to-list 'org-modules 'org-habit)
(add-to-list 'org-modules 'org-crypt)
(add-to-list 'org-modules 'org-id)
(add-to-list 'org-modules 'org-mouse)
(add-to-list 'org-modules 'org-bookmark)
(add-to-list 'org-modules 'org-panel)
;; (add-to-list 'org-modules 'org-screenshot)
;; (add-to-list 'org-modules 'org-wikinodes)
(add-to-list 'org-file-apps '("\\.mp4\\'" . "mplayer %s")) ;; Open videos in mplayer
(add-hook 'org-after-refile-insert-hook  'org-save-all-org-buffers)  ;; Save Org buffers after refiling!

;; Ensure that anything that should be fixed-pitch in Org files appears that way
;; (require 'org-indent)
;; (set-face-attribute 'org-block nil :inherit 'fixed-pitch)
;; (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
;; (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
;; (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
;; (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
;; (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
;; (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
;; (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch) 
;; (set-face-attribute 'org-drawer nil :inherit 'fixed-pitch) 
(add-to-list 'org-latex-packages-alist '("" "tikz" t))

(defun log-todo-next-creation-date (&rest ignore)
  "Log NEXT creation time in the property drawer under the key 'ACTIVATED'"
  (when (and (string= (org-get-todo-state) "NEXT")
	     (not (org-entry-get nil "ACTIVATED")))
    (org-entry-put nil "ACTIVATED" (format-time-string "[%Y-%m-%d]"))))
(add-hook 'org-after-todo-state-change-hook #'log-todo-next-creation-date)

(defun vs/skip-habit-category ()
  (if (equal (org-get-category) "habit")
      (progn
	(outline-next-heading)
	(1- (point)))))

:hook (org-mode . vs/org-mode-setup))
;; (define-key global-map (kbd "C-c j")
;;             (lambda () (interactive) (org-capture nil "j")))

(use-package org-latex-preview
  :ensure nil
  ;; Turn on auto-mode, it's built into Org and much faster/more featured than
  ;; org-fragtog. (Remember to turn off/uninstall org-fragtog.)
  :hook (org-mode-hook . org-latex-preview-auto-mode)
  :config
  ;; Increase preview width
  (plist-put org-latex-preview-appearance-options
             :page-width 0.8)

  ;; Use dvisvgm to generate previews
  ;; You don't need this, it's the default:
  (setq org-latex-preview-process-default 'dvisvgm)

  ;; Block C-n, C-p etc from opening up previews when using auto-mode
  (setq org-latex-preview-auto-ignored-commands
	'(next-line previous-line mwheel-scroll
		    scroll-up-command scroll-down-command meow-next meow-prev))

  ;; Enable consistent equation numbering
  (setq org-latex-preview-numbered t)

  ;; Bonus: Turn on live previews.  This shows you a live preview of a LaTeX
  ;; fragment and updates the preview in real-time as you edit it.
  ;; To preview only environments, set it to '(block edit-special) instead
  (setq org-latex-preview-live t)

  ;; More immediate live-previews -- the default delay is 1 second
  (setq org-latex-preview-live-debounce 0.25))

(use-package org-modern
  ;; :after org
  :custom-face (org-modern-symbol (nil :family "Iosevka Comfy"))
  ;; :custom
  ;; (org-modern-star 'replace)
  :init
  (global-org-modern-mode)
  )

(use-package org-super-agenda
  :custom
  (org-super-agenda-groups '(
				 (:name "Today"
					:time-grid t
					:deadline today
					:scheduled today)
				 (:name "Past"
					:deadline past
					:scheduled past)
				 (:name "Important"
					:priority "A")))

  :hook (org-agenda-mode-hook . org-super-agenda-mode))

(use-package visual-fill-column
  :custom
  (visual-fill-column-center-text t)
  (visual-fill-column-fringes-outside-margins nil)
  :hook org-mode)

(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (lua . t)
     (C . t)
     (latex . t)
     (java . t)
     (shell . t)
     (dot . t)))

  (setq org-babel-python-command "python3") ;; For linux systems
  (push '("conf-unix" . conf-unix) org-src-lang-modes)
  )

(with-eval-after-load 'org
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python"))
  (add-to-list 'org-structure-template-alist '("pas" . "src pascal"))
  (add-to-list 'org-structure-template-alist '("dot" . "src dot"))
  )

(use-package ob-async
  :commands org-babel-execute-src-block:async)

(setq org-image-actual-width (list 550))

(use-package hide-mode-line)

(defun efs/presentation-setup ()
  ;; Hide the mode line
  (hide-mode-line-mode 1)
  ;; Display images inline
  (org-display-inline-images) ;; Can also use org-startup-with-inline-images
  ;; Scale the text.  The next line is for basic scaling:
  (setq text-scale-mode-amount 3)
  ;;(text-scale-mode 1))
  ;; This option is more advanced, allows you to scale other faces too
  (setq-local face-remapping-alist '((default (:height 2.0) variable-pitch)
				       (org-verbatim (:height 1.75) org-verbatim)
				       (org-block (:height 1.25) org-block))))

(defun efs/presentation-end ()
  ;; Show the mode line again
  (hide-mode-line-mode 0)
  ;; Turn off text scale mode (or use the next line if you didn't use text-scale-mode)
  ;;(text-scale-mode 0))
  ;; If you use face-remapping-alist, this clears the scaling:
  (setq-local face-remapping-alist '((default variable-pitch default))))

(use-package org-tree-slide
  :hook ((org-tree-slide-play . efs/presentation-setup)
	   (org-tree-slide-stop . efs/presentation-end))
  :custom
  (org-tree-slide-slide-in-effect t)
  (org-tree-slide-activate-message "Presentation started!")
  (org-tree-slide-deactivate-message "Presentation finished!")
  (org-tree-slide-header t)
  (org-tree-slide-breadcrumbs " > ")
  (org-image-actual-width nil))

(use-package org-download
  :after org
  :custom
  (org-download-screenshot-method 'flameshot "Use customize-set-variable org-download-screenshot-method with M-x to see all the options")
  (org-download-method 'attachment "Use org attachments")
  :commands (org-download-clipboard org-download-image)
  :hook
  (org-mode-hook . org-download-enable)
  (dired-mode-hook . org-download-enable)
  )

(defvar my-hide-org-meta-line-p nil)
(defun my-hide-org-meta-line ()
  (interactive)
  (setq my-hide-org-meta-line-p t)
  (set-face-attribute 'org-meta-line nil
			:foreground (face-attribute 'default :background)))
(defun my-show-org-meta-line ()
  (interactive)
  (setq my-hide-org-meta-line-p nil)
  (set-face-attribute 'org-meta-line nil :foreground nil))

(defun my-toggle-org-meta-line ()
  (interactive)
  (if my-hide-org-meta-line-p
	(my-show-org-meta-line) (my-hide-org-meta-line)))

(add-hook 'org-tree-slide-play-hook #'my-hide-org-meta-line)
(add-hook 'org-tree-slide-stop-hook #'my-show-org-meta-line)

(use-package edraw-org
  :ensure (edraw-org :host github :repo "misohena/el-easydraw")
  :config
  (edraw-org-setup-default)
  )
;; (with-eval-after-load 'org
;;   (load "/home/vslavkin/.emacs.d/elpa/el-easydraw/edraw-org.el")
;;   (require 'edraw-org)
;;   (edraw-org-setup-default))
;; ;; When
;; using the org-export-in-background option (when using the
;; ;
					  ; asynchronous export function), the following settings are
;; required. This is because Emacs started in a separate process does
;; not load org.el but only ox.el.
;; (with-eval-after-load "ox"
;;   (require 'edraw-org)
;;   (edraw-org-setup-exporter))

(use-package org-xournalpp
  :after org
  :ensure (org-xournalpp :host gitlab :repo "vherrmann/org-xournalpp"
			   :files (:defaults "resources"))
  :hook (org-mode-hook . org-xournalpp-mode))

(use-package org-krita
  :after org
  :ensure (org-krita :host github :repo "lepisma/org-krita")
  :hook (org-mode-hook . org-krita-mode))

(use-package org-excalidraw
  :ensure (org-excalidraw :host github :repo "wdavew/org-excalidraw")
  :custom
  (org-excalidraw-directory "~/Pictures")
  )

(use-package sketch-mode)
;; (quelpa '(sketch-mode :repo "dalanicolai/sketch-mode" :fetcher github))
;; (use-package sketch-mode
;;   :defer t)

(use-package org-noter
  :after org
  :commands org-noter
  :custom
  (org-noter-auto-save-last-location t)
  (org-noter-notes-search-path '("~/Documents/book_notes/"))
  (org-noter-always-create-frame nil)
  ;; (org-noter-use-indirect-buffer t)
  (org-noter-kill-frame-at-session-end nil))

(use-package org-remark
  :bind (;; :bind keyword also implicitly defers org-remark itself.
         ;; Keybindings before :map is set for global-map.
         ("C-c n m" . org-remark-mark)
         ;; ("C-c n l" . org-remark-mark-line)
         :map org-remark-mode-map
         ("C-c n o" . org-remark-open)
         ("C-c n ]" . org-remark-view-next)
         ("C-c n [" . org-remark-view-prev)
         ("C-c n r" . org-remark-remove)
         ("C-c n d" . org-remark-delete))
  ;; Alternative way to enable `org-remark-global-tracking-mode' in
  ;; `after-init-hook'.
  :hook (elpaca-after-init . org-remark-global-tracking-mode)
  ;; :init
  ;; It is recommended that `org-remark-global-tracking-mode' be
  ;; enabled when Emacs initializes. Alternatively, you can put it to
  ;; `after-init-hook' as in the comment above
  ;; (org-remark-global-tracking-mode +1)
  :config
  (use-package org-remark-info :ensure nil :after info :config (org-remark-info-mode +1))
  (use-package org-remark-eww  :ensure nil :after eww  :config (org-remark-eww-mode +1))
  (use-package org-remark-nov  :ensure nil :after nov  :config (org-remark-nov-mode +1))
  (org-remark-create "red-line"
               '(:underline (:color "red" :style wave))
               '(CATEGORY "review" help-echo "Review this")))

(use-package org-roam
  ;; :after org
  :custom
  (org-roam-directory (file-truename "~/org-roam/")) ;;Truename is for symlinks
  (org-roam-completion-everywhere t)
  (org-roam-dailies-directory "journals/")
  (org-roam-capture-templates
   '(("d" "default" plain
      "%?"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n")
      :unnarrowed t)
     ("l" "programming language" plain
      (file "~/.emacs.d/org-roam-templates/ProgrammingLanguageTemplate.org")
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
     ("b" "book notes" plain
      (file "~/.emacs.d/org-roam-templates/BookNoteTemplate.org")
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
     ("p" "project" plain
      (file "~/.emacs.d/org-roam-templates/ProjectTemplate.org")
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: Project")
      :unnarrowed t)
     ("w" "what is" plain
      ;; (file (expand-file-name "org-roam-templates/WhatIsTemplate.org" user-emacs-directory))
      (file "~/.emacs.d/org-roam-templates/WhatIsTemplate.org")
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: What")
      :unnarrowed t)
     ("e" "encrypted" plain "%?"
      :target (file+head "${slug}.org.gpg"
			 ";; -*- epa-file-encrypt-to: nil -*-\n#+title: ${title}\n")
      :unnarrowed t)))
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n#+filetags: Daily"))))

  :bind (:prefix-map org-roam-prefix-map
		     :prefix "C-c n"
		     ("f" . org-roam-node-find)
		     ("r" . org-roam-node-random)
		     ("g" . org-roam-graph)

		     ("l" . org-roam-buffer-toggle)
		     ("i" . org-roam-node-insert)
		     ("c" . org-roam-capture)
		     ("o" . org-get-id-create) 
		     ("a" . org-roam-alias-add)
		     ;; Dailies
		     ("j" . org-roam-dailies-capture-today)
		     :prefix "C-c n t"
		     :prefix-map org-roam-tag-prefix-map
		     ("a" . org-roam-tag-add)
		     ("d" . org-roam-tag-remove))
  :config
  ;; (bind-key "o" #'org-id-get-create org-roam-prefix-map);; Useful for making headers their own nodes
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  (cl-defmethod org-roam-node-backlinkscount ((node org-roam-node))
    (let* ((count (caar (org-roam-db-query
			 [:select (funcall count source)
				  :from links
				  :where (= dest $s1)
				  :and (= type "id")]
			 (org-roam-node-id node)))))
      (format "[%d]" count)))
  (cl-defmethod org-roam-node-hierarchy ((node org-roam-node))
    (let ((level (org-roam-node-level node)))
      (concat
       (when (> level 0) (concat (org-roam-node-file-title node) " > "))
       (when (> level 1) (concat (string-join (org-roam-node-olp node) " > ") " > "))
       (org-roam-node-title node))))

  (setq org-roam-node-display-template
	(concat "${hierarchy:*} " (propertize "${tags:40}  " 'face 'org-tag) "${backlinkscount:6}")
	org-roam-node-annotation-function
	(lambda (node) (marginalia--time (org-roam-node-file-mtime node))))
  ;; If using org-roam-protocol
  (require 'org-roam-protocol)
  ;; (require 'org-roam-dailies)
  )

(use-package org-roam-ui
  :ensure (org-roam-ui :host github :repo "odomanov/org-roam-ui")
  :after (org-roam-ui-mode org-roam)
  :custom
  (org-roam-ui-sync-theme t)
  (org-roam-ui-follow t)
  (org-roam-ui-update-on-save t)
  (org-roam-ui-open-on-start t))

(use-package consult-org-roam
  :after (org-roam consult)
  :init
  ;; (require 'consult-org-roam)
  ;; Activate the minor mode
  (consult-org-roam-mode 1) 
  :custom
  ;; Use `ripgrep' for searching with `consult-org-roam-search'
  (consult-org-roam-grep-func #'consult-ripgrep)
  ;; Configure a custom narrow key for `consult-buffer'
  (consult-org-roam-buffer-narrow-key ?r)
  ;; Display org-roam buffers right after non-org-roam buffers
  ;; in consult-buffer (and not down at the bottom)
  (consult-org-roam-buffer-enabled nil)
  (consult-org-roam-buffer-after-buffers t)
  :config
  ;; Eventually suppress previewing for certain functions
  (consult-customize
   consult-org-roam-forward-links
   :preview-key "M-.")
  (add-to-list 'consult-buffer-sources 'org-roam-buffer-source 'append)
  :bind
  ;; Define some convenient keybindings as an addition
  ("C-c n e" . consult-org-roam-file-find)
  ("C-c n b" . consult-org-roam-backlinks)
  ("C-c n l" . consult-org-roam-forward-links)
  ("C-c n r" . consult-org-roam-search))

(use-package org-web-tools
  :after org)

(use-package org-sticky-header
  :after org
  :hook org-mode)

(use-package org-appear
  :ensure (org-appear :host github :repo "awth13/org-appear" :branch "org-9.7-fixes")
  :after org
  :custom
  (org-appear-autoemphasis t)
  (org-appear-autoentities t)
  (org-appear-autosubmarkers t)
  (org-appear-inside-latex nil)
  (org-appear-autolinks t)
  :hook org-mode)

(use-package org-inline-anim
  :hook org-mode)

(defun vs/launch-app ()
  (interactive)
  (if (executable-find "rofi")
      (call-process-shell-command "rofi -show combi")
    (let ((command (read-shell-command "$ ")))
      (start-process-shell-command command nil command))))

(use-package exwm
  ;; :demand
  ;; :ensure nil
  :commands exwm-enable
  ;; :defer nil
  :bind (:map exwm-mode-map
	      ("C-q" . exwm-input-send-next-key))
  ;; C-q pass through the next key
  :custom
  ;; Automatically move EXWM buffer to current workspace when selected
  (exwm-layout-show-all-buffers t)
  ;; Display all EXWM buffers in every workspace buffer list
  (exwm-workspace-show-all-buffers t)
  (exwm-systemtray-height 20)
  :config
  ;;Ugly fix to focus chromium. May use more battery as it's always focused.
  ;; (advice-add #'exwm-layout--hide
  ;; 	      :after (lambda (id)
  ;; 		       (with-current-buffer (exwm--id->buffer id)
  ;; 			 (setq exwm--ewmh-state
  ;; 			       (delq xcb:Atom:_NET_WM_STATE_HIDDEN exwm--ewmh-state))
  ;; 			 (exwm-layout--set-ewmh-state id)
  ;; 			 (xcb:flush exwm--connection))))

  ;; Set the initial workspace number.
  (unless (get 'exwm-workspace-number 'saved-value)
    (setq exwm-workspace-number 5))

  ;; Make class name the buffer name (like "firefox")
  (add-hook 'exwm-update-class-hook
	    (lambda ()
	      (exwm-workspace-rename-buffer exwm-class-name)))

  ;; Global keybindings.
  (unless (get 'exwm-input-global-keys 'saved-value)
    (setq exwm-input-global-keys
	  `(
	    ;; Move between windows
	    ([s-left] . windmove-left)
	    ([s-right] . windmove-right)
	    ([s-up] . windmove-up)
	    ([s-down] . windmove-down)
	    ;; Reset to line-mode (C-c C-k switches to char-mode via exwm-input-release-keyboard)
	    ([?\s-r] . exwm-reset)
	    ;; 's-w': Switch workspace.
	    ([?\s-w] . exwm-workspace-switch) 
	    ;; 's-&': Launch application.
	    ([?\s-&] . vs/launch-app)

	    ;; 's-N': Switch to certain workspace.
	    ,@(mapcar (lambda (i)
			`(,(kbd (format "s-%d" i)) .
			  (lambda ()
			    (interactive)
			    (exwm-workspace-switch-create ,i))))
		      (number-sequence 0 9)))))

  ;; These keys should always pass through to Emacs
  (setq exwm-input-prefix-keys
	'(?\C-x
	  ?\C-u
	  ?\C-h
	  ?\M-x
	  ?\M-`
	  ?\M-&
	  ?\M-:
	  ?\C-\M-j  ;; Buffer list
	  ?\C-\\
	  XF86MonBrightnessUp
	  XF86MonBrightnessDown
	  XF86AudioLowerVolume
	  XF86AudioRaiseVolume
	  XF86AudioPlay
	  XF86AudioStop
	  XF86AudioPrev
	  XF86AudioNex))  ;; Ctrl+Space

  ;; Line-editing shortcuts
  (unless (get 'exwm-input-simulation-keys 'saved-value)
    (setq exwm-input-simulation-keys
	  '(([?\C-b] . [left])
	    ([?\C-f] . [right])
	    ([?\C-p] . [up])
	    ([?\C-n] . [down])
	    ([?\C-a] . [home])
	    ([?\C-e] . [end])
	    ([?\M-v] . [prior])
	    ([?\C-v] . [next])
	    ([?\C-d] . [delete])
	    ([?\C-k] . [S-end delete]))))

  ;; Enable EXWM
  ;; (exwm-enable)

  ;; Detach the minibuffer (show it with exwm-workspace-toggle-minibuffer)
  ;; (setq exwm-workspace-minibuffer-position 'bottom)

  (defun efs/run-in-background (command)
    (let ((command-parts (split-string command "[ ]+")))
      (apply #'call-process `(,(car command-parts) nil 0 nil ,@(cdr command-parts)))))

  (defun efs/exwm-init-hook ()
    ;; Ensure screen updates with xrandr will refresh EXWM frames
    (require 'exwm-randr)
    (setq exwm-randr-workspace-monitor-plist '(0 "eDP-1-1"))
    (defun exwm-change-screen-hook ()
      (let ((xrandr-output-regexp "\n\\([^ ]+\\) connected ")
	    default-output)
	(with-temp-buffer
	  (call-process "xrandr" nil t nil)
	  (goto-char (point-min))
	  (re-search-forward xrandr-output-regexp nil 'noerror)
	  (setq default-output (match-string 1))
	  (forward-line)
	  (if (not (re-search-forward xrandr-output-regexp nil 'noerror))
	      (call-process "xrandr" nil nil nil "--output" default-output "--auto")
	    (call-process
	     "xrandr" nil nil nil
	     "--output" (match-string 0) "--primary" "--auto"
	     "--output" default-output "--off")
	    (setq exwm-randr-workspace-output-plist (list 0 (match-string 0)))))))

    (add-hook 'exwm-randr-screen-change-hook
	      (lambda ()
		(start-process-shell-command
		 "xrandr" nil "xrandr --output eDP1-1 --primary -s 1920x1080 --output HDMI-0 --same-as eDP-1-1 --auto")))
    ;; (add-hook 'exwm-randr-screen-change-hook #'exwm-change-screen-hook)
    (exwm-randr-mode)

    ;; Set the screen resolution
    (start-process-shell-command "xrandr" nil "")
    
    (setq x-no-window-manager t)

    ;; System tray on bottom left corner
    (require 'exwm-systemtray)
    (exwm-systemtray-mode)

    ;; let's get encryption established
    (setenv "GPG_AGENT_INFO" nil)  ;; use emacs pinentry
    (setq auth-source-debug t)
    (setq epa-pinentry-mode 'loopback)
    (setq epg-pinentry-mode 'loopback)
    (require 'org-crypt)
    (org-crypt-use-before-save-magic)

    ;; Make workspace 1 be the one where we land at startup
    (exwm-workspace-switch-create 1)
    ;; Launch apps that will run in the background, DEPENDS ON DEX
    ;; (efs/run-in-background "dex -e EXWM -a")

    (display-time-mode t)
    (let ((battery-str (battery)))
      (unless (or (equal "Battery status not available" battery-str)
		  (string-match-p (regexp-quote "N/A") battery-str))
	(display-battery-mode 1)))

    (setenv "GTK_IM_MODULE" "xim")
    (setenv "QT_IM_MODULE" "xim")
    (setenv "XMODIFIERS" "@im=exwm-xim")
    (setenv "CLUTTER_IM_MODULE" "xim") 
    (setenv "_JAVA_AWT_WM_NONREPARENTING" "1")
    (efs/run-in-background "xsetroot -cursor_name left_ptr")
    (efs/run-in-background "xset r rate 200 60")
    ;; (server-start)

    ;; using xim input
    (require 'exwm-xim)
    (exwm-xim-mode)
    (push ?\C-\\ exwm-input-prefix-keys)   ;; use Ctrl + \ to switch input method
    )
  
  ;; When EXWM finishes initialization, do some extra setup
  (add-hook 'exwm-init-hook #'efs/exwm-init-hook)

  (defun vs/limit-string-size (input-string max-size)
    "Limit the size of INPUT-STRING to MAX-SIZE."
    (if ( <= (length input-string) max-size)
	input-string
      (concat (substring input-string 0 (- max-size 3)) "..." )))
  (defvar vs/max-modeline-str-len 70)

  (defun vs/exwm-update-title ()
    (pcase exwm-class-name
      ("firefox" (exwm-workspace-rename-buffer
		  (vs/limit-string-size (format "Firefox: %s" exwm-title) vs/max-modeline-str-len )))))
  ;; When window title updates, use it to set the buffer name
  (add-hook 'exwm-update-title-hook #'vs/exwm-update-title)

  (defun vs/configure-window-by-class ()
    (interactive)
    (message "Window '%s' appeared!" exwm-class-name))
  ;; Configure windows as they're created
  (add-hook 'exwm-manage-finish-hook #'vs/configure-window-by-class)
  ;; (set-frame-parameter (selected-frame) 'fullscreen 'maximized)
  ;; (add-to-list 'default-frame-alist '(fullscreen . maximized))
  )

(use-package pinentry
  :after exwm
  :config (pinentry-start))

(use-package exwm-edit
  :after exwm
  :bind ("C-c '" . exwm-edit--compose)
  ;; :config (global-exwm-edit-mode +1)
  )

(use-package crux)

(use-package pdf-tools
  :mode("\\.pdf\\'" . pdf-view-mode)
  :config
  (pdf-tools-install)
  (setq-default pdf-view-display-size 'fit-page)
  (setq pdf-annot-activate-created-annotations t))

(use-package saveplace-pdf-view
  :after pdf-tools
  :init (save-place-mode 1)
  ;; :hook (pdf-view-mode-hook . save-place-mode)
  )

(use-package nov
  :mode ("\\.epub\\'" . nov-mode)
  )

(use-package nov-xwidget
  :ensure (nov-xwidget :host github :repo "chenyanming/nov-xwidget")
  :after nov
  :config
  (define-key nov-mode-map (kbd "o") 'nov-xwidget-view)
  (add-hook 'nov-mode-hook 'nov-xwidget-inject-all-files))

(use-package calibredb
  :config
  (setq calibredb-size-show t)
  (setq calibredb-format-character-icons t)
  (setq calibredb-root-dir "~/calibre_lib/")
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  (setq calibredb-library-alist '((calibredb-root-dir)
				    ("~/libros")))
  (defvar calibredb-show-mode-map
    (let ((map (make-sparse-keymap)))
	(define-key map "?" #'calibredb-entry-dispatch)
	(define-key map "o" #'calibredb-find-file)
	(define-key map "O" #'calibredb-find-file-other-frame)
	(define-key map "V" #'calibredb-open-file-with-default-tool)
	(define-key map "s" #'calibredb-set-metadata-dispatch)
	(define-key map "e" #'calibredb-export-dispatch)
	(define-key map "q" #'calibredb-entry-quit)
	(define-key map "y" #'calibredb-yank-dispatch)
	(define-key map "," #'calibredb-quick-look)
	(define-key map "." #'calibredb-open-dired)
	(define-key map "\M-/" #'calibredb-rga)
	(define-key map "\M-t" #'calibredb-set-metadata--tags)
	(define-key map "\M-a" #'calibredb-set-metadata--author_sort)
	(define-key map "\M-A" #'calibredb-set-metadata--authors)
	(define-key map "\M-T" #'calibredb-set-metadata--title)
	(define-key map "\M-c" #'calibredb-set-metadata--comments)
	map)
    "Keymap for `calibredb-show-mode'.")

  (defvar calibredb-search-mode-map
    (let ((map (make-sparse-keymap)))
	(define-key map [mouse-3] #'calibredb-search-mouse)
	(define-key map (kbd "<RET>") #'calibredb-find-file)
	(define-key map "?" #'calibredb-dispatch)
	(define-key map "a" #'calibredb-add)
	(define-key map "A" #'calibredb-add-dir)
	(define-key map "c" #'calibredb-clone)
	(define-key map "d" #'calibredb-remove)
	(define-key map "D" #'calibredb-remove-marked-items)
	(define-key map "j" #'calibredb-next-entry)
	(define-key map "k" #'calibredb-previous-entry)
	(define-key map "l" #'calibredb-virtual-library-list)
	(define-key map "L" #'calibredb-library-list)
	(define-key map "n" #'calibredb-virtual-library-next)
	(define-key map "N" #'calibredb-library-next)
	(define-key map "p" #'calibredb-virtual-library-previous)
	(define-key map "P" #'calibredb-library-previous)
	(define-key map "s" #'calibredb-set-metadata-dispatch)
	(define-key map "S" #'calibredb-switch-library)
	(define-key map "o" #'calibredb-find-file)
	(define-key map "O" #'calibredb-find-file-other-frame)
	(define-key map "v" #'calibredb-view)
	(define-key map "V" #'calibredb-open-file-with-default-tool)
	(define-key map "," #'calibredb-quick-look)
	(define-key map "." #'calibredb-open-dired)
	(define-key map "y" #'calibredb-yank-dispatch)
	(define-key map "b" #'calibredb-catalog-bib-dispatch)
	(define-key map "e" #'calibredb-export-dispatch)
	(define-key map "r" #'calibredb-search-refresh-and-clear-filter)
	(define-key map "R" #'calibredb-search-clear-filter)
	(define-key map "q" #'calibredb-search-quit)
	(define-key map "m" #'calibredb-mark-and-forward)
	(define-key map "f" #'calibredb-toggle-favorite-at-point)
	(define-key map "x" #'calibredb-toggle-archive-at-point)
	(define-key map "h" #'calibredb-toggle-highlight-at-point)
	(define-key map "u" #'calibredb-unmark-and-forward)
	(define-key map "i" #'calibredb-edit-annotation)
	(define-key map (kbd "<DEL>") #'calibredb-unmark-and-backward)
	(define-key map (kbd "<backtab>") #'calibredb-toggle-view)
	(define-key map (kbd "TAB") #'calibredb-toggle-view-at-point)
	(define-key map "\M-n" #'calibredb-show-next-entry)
	(define-key map "\M-p" #'calibredb-show-previous-entry)
	(define-key map "/" #'calibredb-search-live-filter)
	(define-key map "\M-t" #'calibredb-set-metadata--tags)
	(define-key map "\M-a" #'calibredb-set-metadata--author_sort)
	(define-key map "\M-A" #'calibredb-set-metadata--authors)
	(define-key map "\M-T" #'calibredb-set-metadata--title)
	(define-key map "\M-c" #'calibredb-set-metadata--comments)
	map)
    "Keymap for `calibredb-search-mode'.")

  )

(use-package jinx
  :ensure nil
  :config
  (global-jinx-mode)
  :bind (
	 ("M-$" . jinx-correct)
	 ("C-M-$" . jinx-languages)
	 )
  )

(use-package ispell
  :ensure nil
  :custom
  (ispell-program-name "hunspell")
  (ispell-dictionary "en_US")
  (ispell-alternate-dictionary (expand-file-name "dicts/English.dic" user-emacs-directory))
  ;; (ispell-personal-dictionary "~/.hunspell_personal")
  :config
  ;; Configure `LANG`, otherwise ispell.el cannot find a 'default
  ;; dictionary' even though multiple dictionaries will be configured
  ;; in next line.
  ;; (setenv "LANG" "en_US")
  ;; ispell-set-spellchecker-params has to be called
  ;; before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic ispell-dictionary)
  )

(use-package emms
  :ensure t
  :commands emms
  :custom-face
  (emms-browser-artist-face (nil :inherit org-level-1))
  (emms-browser-album-face (nil :inherit org-level-2))
  (emms-browser-album-face (nil :inherit org-level-2))
  
  :custom
  (emms-browser-covers #'emms-browser-cache-thumbnail-async)
  (emms-playlist-mode-center-current t)
  (emms-info-functions '(emms-info-native emms-info-metaflac emms-info-tinytag))
  (emms-source-file-default-directory (expand-file-name "~/Music/"))
  (emms-info-asynchronously t)
  (emms-lyrics-scroll-p nil)
  (emms-lyrics-display-on-modeline nil)
  (emms-lyrics-display-on-minibuffer t)
  (emms-playing-time-mode nil)
  ;; (emms-mode-line-mode t)
  :config
  (require 'emms-setup)
  (emms-all)
  (emms-default-players)

  (require 'emms-mpris)
  (emms-mpris-enable)
  ;; (add-to-list 'emms-player-list 'emms-player-mpd)
  ;; (setq emms-playlist-buffer-name "*Music*")

  ;; Choose one of these
  ;; (require 'emms-info-native)
  ;; (require 'emms-info-libtag)
  ;; (setq emms-info-functions '(emms-info-tinytag))  ;; When using Tinytag
  ;;'emms-info-exiftool When using Exiftool
  ;; (setq emms-info-functions '(emms-info-libtag)) ;;; make sure libtag is the only thing delivering metadata

  (require 'emms-mode-line)

  ;; (require 'emms-playing-time)

  (require 'emms-volume)

  ;; history
  (require 'emms-history)
  (emms-history-load)
  )

(use-package atomic-chrome
  :ensure (atomic-chrome :host github :repo "KarimAziev/atomic-chrome")
  :config (atomic-chrome-start-server))

(use-package comint-mime
  :ensure (:files (:defaults "comint-mime.sh" "comint-mime.py" (:exclude ".gitignore")))
  :hook
  (shell-mode-hook . comint-mime-setup)
  (inferior-python-mode-hook . comint-mime-setup)
  )

(use-package elfeed
  :bind ("C-c w" . elfeed)
  :commands elfeed
  ;; :custom (elfeed-feeds '(
  ;; 			  ("http://nullprogram.com/feed/" emacs)
  ;; 			  ("https://planet.emacslife.com/atom.xml" emacs)))
  :config
  (load-file (expand-file-name "elfeed.el" user-emacs-directory))
  )

(use-package elfeed-org
  :after elfeed
  :disabled
  :demand
  :custom (rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org"))
  :config (elfeed-org))

(use-package elfeed-tube
  :after elfeed
  :bind (
	 :map elfeed-show-mode-map
	 ("F" . elfeed-tube-fetch)
	 ([remap save-buffer] . elfeed-tube-save)
	 :map elfeed-search-mode-map
	 ("F" . elfeed-tube-fetch)
	 ([remap save-buffer] . elfeed-tube-save)
	 :map embark-url-map
	 ("Y" . elfeed-tube-fetch))

  :config
  ;; (setq elfeed-tube-auto-save-p nil) ; default value
  ;; (setq elfeed-tube-auto-fetch-p t)  ; default value
  (elfeed-tube-setup))

(use-package elfeed-tube-mpv
  :bind (:map elfeed-show-mode-map
	      ("C-c C-f" . elfeed-tube-mpv-follow-mode)
	      ("C-c C-w" . elfeed-tube-mpv-where)))

(use-package elfeed-summary
  :after elfeed
  :bind (
	 :map elfeed-search-mode-map
	 ("t" . elfeed-summary)
	 :map elfeed-summary-mode-map
	 ("t" . elfeed))
  :custom
  (elfeed-summary-settings
   '((auto-tags (:original-order . t)))
   ))

(use-package multi-buffer
  :ensure (multi-buffer :host gitlab :repo "vslavkin/multi-buffer.el")
  :init
  (defun multi-vterm-alternative ()
    (interactive)
    (with-current-buffer (vterm) (multi-buffer-mode)))
  (defun multi-eat ()
    (interactive)
    (with-current-buffer (eat) (multi-buffer-mode))
    ))

(use-package eat
  ;; :straight (eat :files (:defaults "terminfo" "integration"))
  :ensure (eat :files (:defaults "eat.ti" "terminfo" "integration"))
  :commands eat
  :custom
  (eat-kill-buffer-on-exit t))

(use-package vterm
  :commands vterm)

(use-package esup
  :commands esup)

(use-package desktop-environment
  :bind-keymap
  ("C-c d" . desktop-environment-mode-map)
  :bind (
	 :map desktop-environment-mode-map
         :prefix-map desktop-environment-prefix
         :prefix "C-c d"
         ("p" . desktop-environment-toggle-music)
         ("f" . desktop-environment-music-next)
         ("b" . desktop-environment-music-previous)
         ("s s" . desktop-environment-screenshot)
         ("s a" . desktop-environment-screenshot-part)
         ("SPC" . desktop-environment-toggle-microphone-mute))
  :init
  (desktop-environment-mode)
  :config
  (advice-add 'desktop-environment-toggle-music :override
  	      (lambda ()
		(interactive)
		(start-process "playerctl" nil "playerctl" "play-pause"))))

(use-package powerthesaurus
  :bind (:map special-insert-map
		("t" . powerthesaurus-transient)))

(use-package docker
  :commands docker)

(use-package disk-usage
  :commands disk-usage)

(use-package pocket-reader)

(use-package lilypond-mode
  :ensure nil
  :mode "\\.ly\\'"
  :commands LilyPond-mode)

(use-package ellama
  ;; :bind-keymap
  ;; ("C-c e" . ellama-command-map)
  ;; :custom
  ;; (ellama-keymap-prefix "C-c e")
  )

(use-package gptel
  :commands (gptel gptel-menu)
  :bind ("C-c g" . gptel-menu)
  :custom
  (gptel-default-mode 'org-mode)
  :config
  (gptel-make-ollama "Ollama"             ;Any name of your choosing
    :host "localhost:11434"               ;Where it's running
    :stream t                             ;Stream responses
    :models '(deepseek-r1:14b qwen2.5:latest gemma2:latest phi4:latest)) ;List of models
  )

(defun my-cleanup-gc ()
  "Clean up gc."
  (setq gc-cons-threshold  104857600) ; 100MiB
  (setq gc-cons-percentage 0.5) ; original value
  (garbage-collect))

(run-with-idle-timer 4 nil #'my-cleanup-gc)

(message "*** Emacs loaded in %s with %d garbage collections."
	   (format "%.2f seconds"
		   (float-time (time-subtract after-init-time before-init-time)))
	   gcs-done)
