(setq package-enable-at-startup nil)

;; @see https://www.reddit.com/r/emacs/comments/ofhket/further_boost_start_up_time_with_a_simple_tweak/
;; 10% speed up of startup for my configuration
(setq gc-cons-percentage 1)
(setq gc-cons-threshold most-positive-fixnum)

(setq inhibit-startup-message t
	frame-resize-pixelwise t ; fine resize
	)

(setq default-frame-alist '((menu-bar-lines . 0)
			      (tool-bar-lines . 0)
			      (horizontal-scroll-bars)
			      (vertical-scroll-bars)))
(set-fringe-mode 10)               ; give some breathing room
(setq read-process-output-max (* 1024 1024)) ;; 1mb, Improves lsp performance
(setenv "LSP_USE_PLISTS" "true") ;;Better lsp performance
